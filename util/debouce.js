function debounce(fn, time) {
	let timer;

	//闭包函数
	return function() {
		if (timer) {
			clearTimeout(timer);
		}
		timer = setTimeout(() => {
			fn()
			timer = undefined;
		}, time)
	}
}