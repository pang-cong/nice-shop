const BASE_URL = ''
export const myRequest = (optios) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + optios.url,
			header: optios.header || {
				Authorization: uni.getStorageSync("token")
			},
			method: optios.method || 'GET',
			data: optios.data || {},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				uni.showToast({
					title: '网络错误',
					icon:'error'
				})
				reject(err)
			}
		})
	})
}