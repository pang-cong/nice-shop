import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)




const tabbarValue = {
	// 存储筛选出来的数据
	value:false
}

// 数据仓库
const state = {
	tabbarValue,
	userid:undefined
}

export default new Vuex.Store({
	state,
	// 同步存储
	mutations:{
		valuemut(state,listdata){

			// 存储到数据仓库
			state.tabbarValue = {
				value:listdata
			}
		},
		userIdValue(state,data){
			state.userid = data
		}
	}
})