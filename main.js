import App from './App'
// 引入封装好的请求
import {
	myRequest
} from 'util/api.js'
Vue.prototype.$myRequest = myRequest

// // 引入socket.io
import io from '@hyoga/uni-socket.io'
// 47.120.42.150
// 127.0.0.1
Vue.prototype.$io = io('http://47.120.42.150:3001', {
	transports: ['websocket'],
	query: `username=${uni.getStorageSync('userId')}`
})

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false

// 引入vuex文件
import store from 'store/store.js'
Vue.prototype.$store = store

App.mpType = 'app'
const app = new Vue({
	...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif

// main.js
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

// 引入拦截器
import "util/interceptor.js"