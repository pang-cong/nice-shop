uni.addInterceptor('request', {
	invoke(args) {
		// request 触发前拼接 url 
		// 远程服务器地址:47.120.42.150
		// console.log("拦截前", args.url);
		if (!args.url.indexOf("https") || !args.url.indexOf("http")) return
		args.url = 'http://47.120.42.150:3001/' + args.url
		// args.url = 'http://localhost:3001/' + args.url

	},
	success(args) {
		if (args.data.status == 401) {
			var page = getCurrentPages()
			uni.showModal({
				title: "请先登陆",
				content: "是否前往登陆页登陆?",
				showCancel: true,
				success(res) {
					if (res.confirm) {
						uni.navigateTo({
							url: "/pages/loginRegistration/loginRegistration",
						})
					} else {
						// console.log("当前路径", page[0].route.indexOf('mine'));
						if (page[0].route.indexOf('mine') != -1 || page[0].route.indexOf('index') != -
							1) {
							return
							uni.navigateBack()
						} else if (page[0].route.indexOf('cart') != -1) {
							uni.switchTab({
								url: "/pages/index/index"
							})
						}
					}
				},
			})
		}
	},
	fail(err) {},
	complete(res) {}
})

uni.addInterceptor("")